package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.api.Task;

import java.util.List;

public class TaskFindAllSortedByDateBeginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-sort-data-begin";
    }

    @Override
    public String getDescription() {
        return "task find all sorted by data begin command";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL TASKS SORTED BY DATE BEGIN]");
        @NotNull final List<Task> taskList = serviceLocator.getTaskEndpoint().findAllSortedByDateBeginTask(serviceLocator.getCurrentSession());
        for (Task task : taskList) {
            System.out.println(task);
        }
    }
}
