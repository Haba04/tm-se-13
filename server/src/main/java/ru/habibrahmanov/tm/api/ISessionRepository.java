package ru.habibrahmanov.tm.api;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.entity.Session;

public interface ISessionRepository {
    @Insert("INSERT INTO app_session (id, user_id, timestamp, signature) " +
            "VALUES (#{id}, #{userId}, #{timestamp}, #{signature})")
    void persist(@NotNull Session session);

    @Delete("DELETE FROM app_session WHERE id = #{id}")
    void remove(@NotNull Session session);
}
