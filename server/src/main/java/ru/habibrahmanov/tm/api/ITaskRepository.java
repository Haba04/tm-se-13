package ru.habibrahmanov.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Task;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public interface ITaskRepository {
    @Insert("INSERT INTO app_task (id, project_id, user_id, name, description, date_begin, date_end, status) " +
            "VALUES (#{id}, #{projectId}, #{userId}, #{name}, #{description}, #{dateBegin}, #{dateEnd}, #{status})")
    void persist(@NotNull Task task);

    @Nullable
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND id = #{projectId}")
    @Results({
            @Result(column = "id", property = "task_id"),
            @Result(column = "projectId", property = "project_id"),
            @Result(column = "userId", property = "user_id"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "dateBegin", property = "date_begin"),
            @Result(column = "dateEnd", property = "date_end"),
            @Result(column = "status", property = "status"),
    })
    Task findOne(@NotNull @Param("taskId") String taskId, @NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM app_task WHERE user_id = #{userId}")
    List<Task> findAllByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM app_task")
    List<Task> findAll();

    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND name LIKE #{string} OR description LIKE #{string}")
    @NotNull List<Task> searchByString (@NotNull @Param("userId") String userId, @NotNull @Param("string") String string);

    @Delete("DELETE FROM app_task WHERE id = #{taskId} AND user_id = #{userId}")
    void remove(@NotNull @Param("userId") String userId, @NotNull @Param("taskId") String taskId);

    @Delete("DELETE FROM app_task WHERE id = #{projectId} AND user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Update("UPDATE app_task SET name = #{name}, description = #{description}, date_begin = #{dateBegin}, date_end = #{dateEnd}, " +
            "status = #{status} WHERE id = #{id} AND user_id = #{userId}")
    void update(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id, @NotNull @Param("name") String name,
                @NotNull @Param("description") String description, @NotNull @Param("status") String status,
                @NotNull @Param("dateBegin") Date dateBegin, @Param("dateEnd") @NotNull Date dateEnd);
}
