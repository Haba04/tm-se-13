package ru.habibrahmanov.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.User;

import java.sql.ResultSet;
import java.util.List;

public interface IUserRepository {
    @Insert("INSERT INTO app_user (id, login, password, role) " +
            "VALUES (#{id}, #{login}, #{password}, #{role})")
    boolean persist(@NotNull User user);

    @Nullable
    @Select("SELECT * FROM app_user WHERE id = #{userId}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "login", property = "login"),
            @Result(column = "password", property = "password"),
            @Result(column = "role", property = "role"),
    })
    User findOne(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM app_user WHERE login = #{login}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "login", property = "login"),
            @Result(column = "password", property = "password"),
            @Result(column = "role", property = "role"),
    })
    User findByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM app_user")
    List<User> findAll();

    @Delete("DELETE FROM app_user WHERE login = #{login}")
    void removeOne(@NotNull @Param("login") String login);

    @Delete("DELETE FROM app_user")
    void removeAll();

    @Update("UPDATE app_user SET login = #{login} WHERE user_id = #{user_id}")
    void update(@NotNull @Param("userId") String userId, @NotNull @Param("login") String login);
}
