package ru.habibrahmanov.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;

import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

public interface IProjectRepository {
    @Insert("INSERT INTO app_project (id, user_id, name, description, date_begin, date_end, status) " +
            "VALUES (#{id}, #{userId}, #{name}, #{description}, #{dateBegin}, #{dateEnd}, #{status})")
    boolean persist(@NotNull Project project);

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM app_project")
    List<Project> findAll();


    @Update("UPDATE app_project SET name = #{name}, description = #{description}, date_begin = #{dateBegin}, date_end = #{dateEnd}, " +
            "status = #{status} WHERE id = #{projectId} AND user_id = #{userId}")
    void update(
            @NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId, @NotNull @Param("name") String name,
            @NotNull @Param("description") String description, @NotNull @Param("status") String status, @NotNull @Param("dateBegin") Date dateBegin,
            @NotNull @Param("dateEnd") Date dateEnd
    );

    @NotNull
    @Select("SELECT * FROM app_project WHERE id = #{projectId} AND name LIKE #{string} OR description LIKE #{string}")
    List<Project> searchByString (@NotNull @Param("projectId") String projectId, @NotNull @Param("string") String string);

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} AND id = #{projectId}")
    @Results({
            @Result(column = "id", property = "project_id"),
            @Result(column = "userId", property = "user_id"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "dateBegin", property = "date_begin"),
            @Result(column = "dateEnd", property = "date_end"),
            @Result(column = "status", property = "status"),
    })
    Project findOne(@NotNull @Param("projectId") String projectId, @NotNull @Param("userId") String userId);

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM app_project WHERE id = #{projectId} AND user_id = #{userId}")
    void remove(@NotNull @Param("projectId") String projectId, @NotNull @Param("userId") String userId);
}
