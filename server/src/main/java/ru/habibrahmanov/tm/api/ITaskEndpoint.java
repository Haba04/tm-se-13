package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    String URL = "http://localhost:8080/TaskEndpoint?wsdl";

    @WebMethod
    void persistTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "task", partName = "task") Task task
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;

    @WebMethod
    void insertTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") String projectId,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description,
            @WebParam(name = "dateBegin", partName = "dateBegin") String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;

    @Nullable
    @WebMethod
    Task findOneTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskId", partName = "taskId") String taskId
            ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
            ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<Task> findAllSortedByDateBeginTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<Task> findAllSortedByDateEndTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws ListIsEmptyExeption, IncorrectValueException, SessionIsNotValidException;

    @NotNull
    @WebMethod
    List<Task> findAllSortedByStatusTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws IncorrectValueException, SessionIsNotValidException, ListIsEmptyExeption;

    @NotNull
    @WebMethod
    List<Task> searchByStringTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "string", partName = "string") String string
    ) throws IncorrectValueException, ListIsEmptyExeption, SessionIsNotValidException;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskId", partName = "taskId") String taskId
    ) throws IncorrectValueException, RemoveFailedException, SessionIsNotValidException;

    @WebMethod
    void removeAllTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "projectId", partName = "projectId") String projectId
    ) throws IncorrectValueException, RemoveFailedException, SessionIsNotValidException;

    @WebMethod
    void updateTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description,
            @WebParam(name = "status", partName = "status") @Nullable String status,
            @WebParam(name = "dateBegin", partName = "dateBegin") @Nullable String dateBegin,
            @WebParam(name = "dateEnd", partName = "dateEnd") @Nullable String dateEnd
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;

    @WebMethod
    void mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "task", partName = "task") @Nullable Task task
    ) throws IncorrectValueException, ParseException, SessionIsNotValidException;
}
