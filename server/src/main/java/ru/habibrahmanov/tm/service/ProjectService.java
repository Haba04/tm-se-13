package ru.habibrahmanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.comporator.ProjectDateBeginComparator;
import ru.habibrahmanov.tm.comporator.ProjectDateEndComparator;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.util.MyBatisUtil;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public final class ProjectService extends AbstractService implements IProjectService {

    @Nullable private final MyBatisUtil myBatisUtil = new MyBatisUtil();
    @Nullable private final SqlSessionFactory sessionFactory = myBatisUtil.getSqlSessionFactory();

    @Override
    public void insert(
            @Nullable final String name, @Nullable final String description, @Nullable final String dateBegin,
            @Nullable final String dateEnd, @Nullable final String userId
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if(projectRepository.persist(new Project(UUID.randomUUID().toString(), userId, name, description,
                    dateFormat.parse(dateBegin), dateFormat.parse(dateEnd)))){
                sqlSession.commit();
            }
            sqlSession.rollback();

        }
    }

    @Override
    public void persist(@Nullable final Project project) throws IncorrectValueException {
        if (project == null) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.persist(project)) {
                sqlSession.commit();
            }
            sqlSession.rollback();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        try(@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.findAllByUserId(userId).isEmpty()) throw new ListIsEmptyExeption();
            return projectRepository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() throws ListIsEmptyExeption {
        try(@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
            return projectRepository.findAll();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        try(@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeAll(userId);
            sqlSession.commit();
        }
    }

    @Override
    public void remove(@Nullable final String projectId, @Nullable final String userId) throws IncorrectValueException, RemoveFailedException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        try(@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.remove(projectId, userId);
            sqlSession.commit();
        }
    }

    @Override
    public void update(
            @Nullable final String userId, @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable final String status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        try(@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.update(userId, projectId, name, description, status, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
            sqlSession.commit();
        }
    }

    @Override
    public void merge(
            @NotNull final Project project
    ) throws IncorrectValueException, ParseException {
        try(@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.findOne(project.getId(), project.getUserId()) == null) {
                persist(project);
                sqlSession.commit();
            } else {
                update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getStatus().displayName(),
                        dateFormat.format(project.getDateBegin()), dateFormat.format(project.getDateEnd()));
                sqlSession.commit();
            }
        }
    }

    @NotNull
    @Override
    public List<Project> searchByString (
            @Nullable final String userId, @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        try(@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.searchByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
            return projectRepository.searchByString(userId, string);
        }
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByDateBegin(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Project> projectList = findAll(userId);
        Collections.sort(projectList, new ProjectDateBeginComparator());
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByDateEnd(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Project> projectList = findAll(userId);
        Collections.sort(projectList, new ProjectDateEndComparator());
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStatus(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Project> projectList = findAll(userId);
        Collections.sort(projectList);
        return projectList;
    }
}
