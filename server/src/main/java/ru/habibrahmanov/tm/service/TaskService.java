package ru.habibrahmanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.comporator.TaskDateBeginComparator;
import ru.habibrahmanov.tm.comporator.TaskDateEndComparator;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.util.DbConnectUtil;
import ru.habibrahmanov.tm.util.MyBatisUtil;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public final class TaskService extends AbstractService implements ITaskService {

    @Nullable private final MyBatisUtil myBatisUtil = new MyBatisUtil();
    @Nullable private final SqlSessionFactory sessionFactory = myBatisUtil.getSqlSessionFactory();

    @Override
    public void persist(@Nullable final Task task) throws IncorrectValueException {
        if (task == null) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.persist(task);
            sqlSession.commit();
        }
    }

    @Override
    public void insert(
            @Nullable final String projectId, @Nullable final String userId, @Nullable final String name,
            @Nullable final String description, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.persist(
                    new Task(UUID.randomUUID().toString(), projectId, userId, name,
                            description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd))
            );
            sqlSession.commit();
        }
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String taskId, @Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (taskId == null || taskId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.findOne(taskId, userId) == null) throw new ListIsEmptyExeption();
            return taskRepository.findOne(taskId, userId);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.findAllByUserId(userId).isEmpty()) throw new ListIsEmptyExeption();
            return taskRepository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws ListIsEmptyExeption {
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
            return taskRepository.findAll();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws IncorrectValueException, RemoveFailedException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskId == null || taskId.isEmpty()) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.remove(userId, taskId);
            sqlSession.commit();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId, @Nullable final String projectId) throws IncorrectValueException, RemoveFailedException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty())
            try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
                @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                taskRepository.removeAll(userId, projectId);
                sqlSession.commit();
            }
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description,
                       @Nullable final String status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (id == null || id.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(userId, id, name, description, status, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
            sqlSession.commit();
        }
    }

    @Override
    public void merge(
            @NotNull final Task task
    ) throws IncorrectValueException, ParseException {
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.findOne(task.getId(), task.getProjectId()) == null) {
                persist(task);
                sqlSession.commit();
            } else {
                update(task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getStatus().displayName(),
                        dateFormat.format(task.getDateBegin()), dateFormat.format(task.getDateEnd()));
                sqlSession.commit();
            }
        }
    }

    @NotNull
    @Override
    public List<Task> searchByString (
            @Nullable final String userId, @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        try (@Nullable final SqlSession sqlSession = sessionFactory.openSession()) {
            @Nullable final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.searchByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
            return taskRepository.searchByString(userId, string);
        }
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByDateBegin(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Task> taskList = findAll(userId);
        Collections.sort(taskList, new TaskDateBeginComparator());
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByDateEnd(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Task> projectList = findAll(userId);
        Collections.sort(projectList, new TaskDateEndComparator());
        return projectList;
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByStatus(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Task> taskList = findAll(userId);
        Collections.sort(taskList);
        return taskList;
    }
}
