package ru.habibrahmanov.tm.exeption;

public class IncorrectValueException extends Exception {
    public IncorrectValueException() {
        super("YOU ENTERED AN INCORRECT VALUE");
    }

    public IncorrectValueException(String message) {
        super(message);
    }

}
