package ru.habibrahmanov.tm.exeption;

public class SessionIsNotValidException extends Exception {
    public SessionIsNotValidException() {
        super("SESSION IS NOT VALID");
    }

    public SessionIsNotValidException(String message) {
        super(message);
    }
}
