package ru.habibrahmanov.tm.util;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.api.ISessionRepository;
import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.api.IUserRepository;
import ru.habibrahmanov.tm.service.PropertyService;

import javax.sql.DataSource;

public class MyBatisUtil {
    @Nullable final PropertyService propertyService = new PropertyService();

    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String login = propertyService.getDbLogin();
        @Nullable final String password = propertyService.getDbPassword();
        @Nullable final String url = propertyService.getUrl();
        @Nullable final String driver = propertyService.getDriver();
        final DataSource dataSource =
                new PooledDataSource(driver, url, login, password);
        final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
