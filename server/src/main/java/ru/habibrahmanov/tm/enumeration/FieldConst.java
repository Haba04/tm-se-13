package ru.habibrahmanov.tm.enumeration;

public enum FieldConst {
    ID,
    NAME,
    DESCRIPTION,
    DATE_BEGIN,
    DATE_END,
    LOGIN,
    PASSWORD;

    @Override
    public String toString() {
        return super.toString();
    }
}
