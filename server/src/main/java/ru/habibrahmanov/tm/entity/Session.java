package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Session implements Cloneable {
    private String id = UUID.randomUUID().toString();
    private String userId;
    private Long timestamp = System.currentTimeMillis();
    private String signature;

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    @NotNull
    public String toString() {
        return "Id \"" + id + "\"" +
                "\nUser Id = " + userId +
                "\nTime stamp = " + timestamp +
                "\n";
    }
}
