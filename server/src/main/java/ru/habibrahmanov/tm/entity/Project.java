package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.enumeration.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project implements Comparable<Project>, Serializable {
    private String id;
    private String userId;
    private String name;
    private String description;
    private Date dateBegin;
    private Date dateEnd;
    private Status status = Status.PLANNED;

    public Project(String id, String userId, String name, String description, Date dateBegin, Date dateEnd) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public int compareTo(@NotNull Project o) {
        return this.getStatus().compareTo(o.getStatus());
    }

    @Override
    @NotNull
    public String toString() {
        return "Project \"" + name + "\"" +
                "\nStatus = " + status.displayName() +
                "\nProject Id = " + id +
                "\nDescription = '" + description + '\'' +
                "\nDateBegin = " + dateBegin.getTime() +
                "\nDateEnd = " + dateEnd.getTime() +
                "\nUser Id = " + userId +
                "\n";
    }
}